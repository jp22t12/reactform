import { useState ,useEffect } from 'react'
import DynamicForm from '../../components/DynamicForm/DynamicForm';
import axios from 'axios';
import './Home.scss'

function Home() {
    const [isLoading, setIsLoading] = useState(true); //Loading until get the data
    const [response, setResponse] = useState();     //Store the api response data

    // Function for get api data
    const fatchApi = async () => {
        await axios
          .get('https://run.mocky.io/v3/4bf1a877-627c-48ef-bf9b-3c50870f28eb')
          .then((response) => {
            if (response.status === 200) {
              setIsLoading(false);  //on successful get data stop the loading
              setResponse(response.data)
            }
          })
          .catch((error) => {
            console.log(error);
          });
      };
      // Useeffect hook for calling fetchapi function on reload.
      useEffect(() => {
        fatchApi();
      }, []);

      // display dynamic form on homepage
  return isLoading ? (
        <span className="loading"><span className="spinner"></span></span>
      ) : (
            <>
            <h1 className='tc heading'>React Form</h1>
            <DynamicForm response={response}/> 
            </>
      )
}

export default Home