import React from 'react'
import { Modal } from 'react-bootstrap'
import "./PopupModal.scss"

const PopupModal = ({isopen,setIsOpen,value}) => {
  // this modal component display the submited data through popup
  return (
    <Modal show={isopen}>
    <Modal.Header>
    <button
        className="close ms-auto btn btn-outline-none"
        onClick={() => setIsOpen(false)}
    >
        X
    </button>
    </Modal.Header>
    <Modal.Body>
        {Object.entries(value).map((data,index)=>{
            return (
              //key(data[0]):value(data[1]) if value is ""empty then dont display it on popup 
              data[1] !== ""
              &&
                <div className="data-container" key={index}>
                        <div className="data">{data[0]}</div>
                        <div className="data">{String(data[1])}</div>
                </div>
              )
        })}
    </Modal.Body>
    </Modal>
  )
}

export default PopupModal