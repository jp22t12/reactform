import React from 'react'

const FormInput = (props) => {
  return (
    <div className={props.class}>
        <input autoComplete="off" type={props.type} placeholder={props.placeholder} name={props.name} onChange={(e)=>props.onChange(e)} onBlur={(e)=>{props.validation(e)}}/>
        {props.error&&<div className='error'>{props.error}</div>}
    </div>
  )
}

export default FormInput