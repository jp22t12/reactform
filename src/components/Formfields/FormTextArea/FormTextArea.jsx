import React from 'react'

const FormTextArea = (props) => {
  return (
    <div className={props.class}>
        <textarea autoComplete="off" placeholder={props.placeholder} name={props.name} onChange={(e)=>props.onChange(e)} onBlur={(e)=>{props.validation(e)}} />
        {props.error&&<div className='error'>{props.error}</div>}
    </div>
  )
}

export default FormTextArea