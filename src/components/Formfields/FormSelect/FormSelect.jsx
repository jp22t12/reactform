import React from "react";

const FormSelect = (props) => {
  //this select component fullfield the cascading dropdown 
  return (
    <div className={props.class}>
      <select
        name={props.name}
        onChange={(e) => {
          //if data have child info then this function set the child value ""(empty) 
          (props.value && props.child)?
          props.onSelChange(e,props.child):
          props.onChange(e)
        }}
        onBlur={(e) => {
          //validation on blur
          props.validation(e);
        }}
      >
        <option value="">{props.placeholder}</option>
        {/* for simple select field there is belongs to is null */}
        {props.belongsto === null
          ? props.options[props.name].map((option) => {
              return (
                <option key={option.id} value={option.name}>
                  {option.name}
                </option>
              );
            })
            
          :
          // if field is belongs to someone then option rander related to that belongsto parent 
          //map the option[selected field name] - thats why we should keep same name of select field name and option array name
          props.options[props.name].map((option) => {
              return (
                // render that option which parent name is equall to parent selected value(country:india)[option.name-vadodara,option.parent-india=selected country:india]
                option.parent === props.fielddata[props.belongsto] && (
                  <option key={option.id} value={option.name}>
                    {option.name}
                  </option>
                )
              );
            })}
      </select>
        {props.error&&<div className='error'>{props.error}</div>}
    </div>
  );
};

export default FormSelect;