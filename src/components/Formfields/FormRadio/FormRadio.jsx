import React from 'react'

const FormRadio = (props) => {
  return (
    <div className={props.class}>
        <input type="radio" name={props.for} value={props.placeholder} onChange={(e)=>props.onChange(e)} onBlur={(e)=>{props.validation(e)}}/>
        <label>{props.placeholder}</label>
        {props.error&&<div className='error'>{props.error}</div>}
    </div>
  )
}

export default FormRadio