import React from 'react'

const FormCheckBox = (props) => {
  return (
    <>
        <div className={props.class}>
          <div className="checkbox-field">
            <input type="checkbox" name={props.name} value={props.value}
            onChange={
              (e)=>{
                props.onChange(e)
              }
              } onBlur={(e)=>{props.validation(e)}}/>
            <label>{props.placeholder}</label>
          </div>
          {props.error&&<div className='error'>{props.error}</div>}
        </div>
    </>
  )
}

export default FormCheckBox