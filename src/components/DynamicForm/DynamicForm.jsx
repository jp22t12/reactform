import React, { useState } from "react";
import PopupModal from "../PopupModal/PopupModal";
import FormSelect from "../Formfields/FormSelect/FormSelect";
import FormRadio from "../Formfields/FormRadio/FormRadio";
import FormInput from "../Formfields/FormInput/FormInput";
import FormCheckBox from "../Formfields/FormCheckBox/FormCheckBox";
import FormTextArea from "../Formfields/FormTextArea/FormTextArea"
import "./DynamicForm.scss";

function DynamicForm({ response }) {
  const [value,setValue] = useState({})     //value contain all value of input field
  const [error,setError] = useState({})     //contain all the error which is not fullfield the requrement.
  const [isopen,setIsOpen] = useState(false)  //use for open modal at the end of submit

  //Validation function for validate all the field on out of focus(onBlur event) [validation onBlur]
  const validation = (e) => {
    response.formdata.map((data)=>{
        if(data.name === e.target.name){
          if(e.target.value==="" && data.required === "required"){
            // if mock data contain reqerror it will show that error or else display default error on required field
            data.reqerror?setError({...error,[e.target.name]:data.reqerror}):
              setError({...error,[e.target.name]:"Field is required"});
          }else{
            //match the pattern with the input value
            if(!e.target.value.match(data.pattern) && data.pattern !== null && e.target.value!==""){
              //add error in error object with the key name = field name
              setError({...error,[e.target.name]:data.error})
            }else{
              //remove the error from error object if all requirements(pattern match,required match,etc.) fullfield
              setError((current) => {
                  const copy = {...current};
                  delete copy[e.target.name];
                  return copy
              })
            }
          }
        }
    })
}

//onsubmit check that error state contain any error and/or required field filled or not [validation on submit with open popup which display the submitted data]
const submitedData = (e) => {
  let errorflag = false;
  let errlist;
  // check the error state contain any error or not if contain then turn the errorflag true and if not contain then check requred fields
  Object.keys(error).length === 0 
  ? response.formdata.map((data)=>{
      if(data.required ==="required"&& !value[data.name]){
          errlist = data.reqerror?{...errlist,[data.name]:data.reqerror}:{...errlist,[data.name]:"Field is required"};
          setError({...error,...errlist})
          errorflag = true
      }
  }):errorflag = true
  // if requrements fullfields is set open true for popup open.
  errorflag? alert("Please fulfill the requirements") : setIsOpen(true)
}

//on change store the value in value state for controlled input
const onChange = (e) => { 
  setValue({...value,[e.target.name]: e.target.type === "checkbox"? e.target.checked : e.target.value});
}

//on chnage of select this function set the chilldren value ""(empty).
const onSelChange = (e,child) => {
  let childs;
  child?.map((child)=>{
      childs = {...childs,[child]:""};
  })
  setValue({...value,[e.target.name]:e.target.value,...childs});
}
  return (
    <>
      <div className="dynamic-form">
        <div className="container">
            <div className="row">
                <div className="col-12 col-md-9 m-auto">
                    <form className='dynamic-form-container'>
                        {
                        response.formdata.map((data)=> {
                            return data.type==="text"?<FormInput key={data.id} class={"form-field"} {...data} value={value[data.name]} onChange={onChange} validation={validation} error={error[data.name]}/>
                            :data.type==="select"?<FormSelect key={data.id} class={"form-field"} {...data} options={response.optiondata} value={value[data.name]} fielddata={value} validation={validation} onSelChange={onSelChange} onChange={onChange} error={error[data.name]}/>
                            :data.type==="radio"?
                            <div className="form-field" key={data.id}>
                              <div className="field-title">
                                {data.name}
                              </div>
                              <div className="field rdbtn">
                                {
                                  data.radiodata.map((data)=>{
                                    return(
                                      <FormRadio key={data.id} class={"form-radio-field"} {...data} validation={validation} onChange={onChange} error={error[data.name]}/>
                                    )
                                  })
                                }
                              </div>
                            </div>
                            :data.type==="textarea"
                            ?<FormTextArea key={data.id} class={"form-field form-field-textarea"} {...data} value={value[data.name]} validation={validation} onChange={onChange} error={error[data.name]}/>
                            :data.type==="checkbox"
                            ?<FormCheckBox key={data.id} class={"form-field form-field-checkbox"} {...data} value={value[data.name]} validation={validation} onChange={onChange} error={error[data.name]}/>
                            :"";
                        })
                        }          
                        <div className='form-btn'><button className="btn btn-outline-dark mt-4 mb-3" type='submit' onClick={(e)=>{e.preventDefault();submitedData(e)}}><b>Submit</b></button></div>
                    </form>
                </div>
            </div>
        </div>
        <PopupModal value={value} isopen={isopen} setIsOpen={setIsOpen}/>
    </div>
      
    </>
  );

}

export default DynamicForm;




// useEffect(() => {
//   setPrevFilter(filteredoptions);
// }, [filteredoptions]);

// //On country,State & City value change it filter out necessary data for select-Options
// const handleChange = (e) => {
//   //if lowest child then seted previous filtered options
//   if (e.target.dataset.children !== "lowestchild") {
//     //(selectedparent for country:undefined, State:countryValue, City:skip as above condition )
//     const selectedparent = document.querySelector(
//       `[data-self='${e.target.dataset.parent}']`
//     )?.value;
//     //(selected for country:countryValue, State:stateValue, City:skip as above condition )
//     const selfselected = document.querySelector(
//       `[data-self='${e.target.dataset.self}']`
//     )?.value;

//     //filter the options based on country,state,city
//     //relatedto is array which is define in option objects in JSON data
//     const filtopt = options.filter((option) => {
//       //if child not contain current selected value in relatedto(array) list then discard that option
//       //for example for gujarat from all city who do not contain gujarat in its relatedto list will be discard.  
//       if (
//         !option.relatedto.includes(e.target.value) &&
//         option.self_id === e.target.dataset.children
//       ) {
//         return false;
//       }else if (
//         //if self not contain parent in its related to list then it will discarded
//         //for example self=st so from all st(state) navsari & vadodara contain selectedparent=india and all others discarded.
//         option.self_id === e.target.dataset.self &&
//         !option.relatedto.includes(selectedparent) &&
//         !option.relatedto.includes(null)
//       ) {
//         return false;
//       } else if (
//         //its connect subchilds means country and city
//         //for example if city not contain india in relatedto(array) list then it will discard 
//         option.self_id === e.target.dataset.subchild &&
//         !option.relatedto.includes(selfselected) &&
//         !option.relatedto.includes(null)
//       ) {
//         return false;
//       }
//       return true;
//     });
//     setFilteredOptions(filtopt);
//   } else {
//     setFilteredOptions(prevfilter);
//   }
// };

// //enable submit button only on check box checked
// const handleButton = (e) => {
//   let take = document.querySelector(".btn");
//   return(
//   e.target.checked === true
//     ? (take.classList.remove("disabled-btn"), (take.disabled = false))
//     : (take.classList.add("disabled-btn"), (take.disabled = true))
//     )
// };


// const validChange = (e) => {
//   let value = e.target.value;
//   if(e.target.dataset.pattern !== null && (!value.match(e.target.dataset.pattern) && e.target.value !== "")){
//       let element = e.target.name;
//       setErrors({elementname: element})
//     }else{
//       setErrors({elementname: ""})
//     }
// }

// dynamic form based on json data
//       <form className="form">
//         {
//           //Map the Json data for Form-Fields
//           response.form.map((data) => {
//             let name = data.name;
//             return (
//               //Puted conditions for diffrent type of html elements
//               data.element === "textfield" ? (
//                 <span key={data.id}>
//                 <input  
//                   name={data.name}
//                   type={data.type}
//                   placeholder={data.placeholder}
//                   className="form-field"
//                   data-pattern={data.pattern}
//                   onChange={validChange}
//                   required
//                 />
//                 {errors.elementname === name && <p className="errormsg">Not Valid</p>}
//                 </span>
//               ) : data.element === "select" ? (
//                 <select
//                   key={data.id}
//                   data-children={data.children}
//                   data-subchild={data.subchild}
//                   data-self={data.opt}
//                   data-parent={data.parent}
//                   className="form-field"
//                   onChange={handleChange}
//                   required
//                 >
//                   <option value="">{data.name}</option>
//                   {
//                     //map options of select field
//                     filteredoptions.map((option) => {
//                       return (
//                       option.self_id.includes(data.opt)&&
//                           <option key={option.id} value={option.name}>
//                             {option.name}
//                           </option>
//                         );
                      
//                     })
//                   }
//                 </select>
//               ) : data.element === "textarea" ? (
//                 <textarea key={data.id} placeholder={data.placeholder} />
//               ) : data.element === "radiobutton" ? (
//                 <label key={data.id}>
//                   <input
//                     className="radio-button"
//                     type={data.type}
//                     name={data.name}
//                   />
//                   {data.label}
//                 </label>
//               ) : data.element === "checkbox" ? (
//                 <div key={data.id}>
//                   <label>
//                     <input
//                       className="checkbox"
//                       type={data.type}
//                       name={data.name}
//                       onClick={handleButton}
//                     />
//                     {data.label}
//                   </label>
//                 </div>
//               ) : (
//                 ""
//               )
//             );
//           })
//         }
//         {/* button */}
//         <div className="btn-container">
//           <button type="submit" className="btn disabled-btn" disabled>
//             Submit
//           </button>
//         </div>
//       </form>