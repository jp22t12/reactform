1. npm install

2. npm start 

3. whole dynamic form generate based on Json data 
    Location of Json data : src/form-data/formdata.json
    online Location: "https://run.mocky.io/v3/4bf1a877-627c-48ef-bf9b-3c50870f28eb"

4. How it work based on data;

    data = {
        "id": 1,                         //item id
        "name": "department",            //item name
        "placeholder": "Department*",    //item placeholder/label for display on site
        "type": "text",                  //item field-type - based on type render diffrent type of field
        "pattern": null,                 //item pattern for validation
        error:"error message"            //error is pass msg if value is not fulfilled with pattern matching. 
        "required": "required"           //item required for checking- is this field compulsary or not?
        reqerror:"must require"          //reqerror is pass msg "must require" if field is not filled (optional-default msg - "Field is required")
        child:[state,city,etc](optional) //this field is for type select which define the children array for that select field(for example [state,city] is childern of country) - The defined children value will empty on chnage of his parent value change.
        belongsto:(null/country/state)   //this field is for type select where any one field is belongsto other (foe example state belongs to country) - its is optional we can make undependant select filed without using this field.
        radiodata:[{radiodata1},{data2}] //this field is for radio button which contain the data of diffrent radio button.(explain below)
    }

5. how to handle radio button:

    data={
        "id": 8, 
        "type": "radio",        //define type of filed
        "name": "gender",       //define name for radio button
        "radiodata": [          //define array of object which contain radiobtn data 
        {   
        "id": 8881,             //radiobtn ID
        "for": "gender",        //for define the name for radio input field   <input name={data.radiodata[0].for}>male    
        "placeholder": "Male"   //placeholder is for label the name for radio button
        },
        {
        "id": 8882,
        "for": "gender",
        "placeholder": "Female"
        },
        {
        "id": 8883,
        "for": "gender",
        "placeholder": "other"
        }
        ]
        },
}

6. Option data for select field

optiondata": {              //option data
    state": [               //define array with same name of field name
        {
        "id": 101,          //option id
        "name": "gujarat",  //option name
        "parent": "india"   //option parent name (optional) use for cascading dropdown 
        },
        {
        "id": 102,
        "name": "maharastra",
        "parent": "india"
        },
        {
        "id": 103,
        "name": "rajasthan",
        "parent": "india"
        },
    ]
}